from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.shortcuts import redirect

#def root_view(request):
#    return redirect('blog.views.index')

urlpatterns = [
    url('^admin/', admin.site.urls),
    url('^blog/', include('blog.urls')),
    url('^accounts/', include('accounts.views')),
    # url(r'^$', root_view),
    url(r'^$', lambda request: redirect('blog.views.index')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
