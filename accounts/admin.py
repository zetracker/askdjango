from django.contrib import admin
from .models import Profile, Follow

admin.site.register(Profile)


class FollowAdmin(admin.ModelAdmin):
	list_display = ('id','from_user','to_user')


admin.site.register(Follow, FollowAdmin)