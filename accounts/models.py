from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.core.validators import RegexValidator
from django import forms
import re

def follow(self, to_user):
    kwargs = {'from_user':self, 'to_user':to_user}
    try:
        Follow.object.creates(**kwargs)
    except IntegrityError:
        pass

def unfollow(self, to_user):
    kwargs = {'from_user':self, 'to_user':to_user}
    unfollow = Follow.objects.filter(**kwargs).delete()

setattr(User, 'follow', follow)
setattr(User, 'unfollow', unfollow);


def phone_validator(value):
    number = ''.join(re.findall(r'\d+', value))
    if not re.match(r'^01[016789]\d{7,8}$', number):
        raise forms.ValidationError('휴대폰 번호를 입력해주세요.')

class PhoneField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 20)
        super(PhoneField, self).__init__(*args, **kwargs)
        self.validators.append(phone_validator)


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    phone = PhoneField()
    birth_year = models.PositiveIntegerField()


class Follow(models.Model): 
    from_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='following_set')
    to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='follower_set')
    is_blocked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [
            ['from_user', 'to_user'],
        ]

