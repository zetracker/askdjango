from django.conf.urls import url
from django.contrib.auth.views import login, logout
from django.shortcuts import render, redirect
from accounts.forms import SignupForm2, LoginForm
from django.contrib.auth.decorators import login_required


def signup(request):
    if request.method == 'POST':
        form = SignupForm2(request.POST)
        if form.is_valid():
            form.save()
            return redirect('blog.views.post_list')
    else:
        form = SignupForm2()

    return render(request, 'accounts/signup.html', {
        'form':form,
    })

@login_required
def profile(request):
    return render(request, 'accounts/profile.html', {

    })


# URL MAPPER
urlpatterns = [
    url(r'^signup/$', signup, name='signup'),
    url(r'^login/$', login, name='login', kwargs={
        'template_name':'accounts/login.html',
        'authentication_form':LoginForm
    }),
    url(r'^logout/$', logout, name='logout'),
    url(r'^profile/$', profile, name='profile'),
]
