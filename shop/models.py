
from django.conf import settings
from django.db import models

# Create your models here.
class Post(models.Model):
	author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='shop_user')
	title = models.CharField(max_length=100)
