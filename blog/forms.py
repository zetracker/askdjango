from django import forms
from .models import Post, Comment, Tag

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title','content', 'photo']


class CommentForm(forms.ModelForm):
    is_agreed = forms.BooleanField(label='약관에 동의합니다', error_messages={
        'required':'약관에 동의하셔야 등록이 가능합니다',
    })

    class Meta:
        model = Comment
        fields = ['message']

    def clean_message(self):
        message = self.cleaned_data.get('message', None)
        if message:
            message = message.strip()
            if len(message) < 10:
                raise forms.ValidationError('10글자 이상 입력하기!!!')

        return message

    def clean(self):
        return super(CommentForm, self).clean()
