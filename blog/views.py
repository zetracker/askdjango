from django.shortcuts import render, redirect, get_object_or_404
from .models import Post
from .forms import PostForm, CommentForm


def index(request):
    return render(request, 'blog/index.html')

def post_list(request):
    post_list = Post.objects.all()

    return render(request, 'blog/post_list.html', {
        'post_list':post_list,
    })

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)

    if request.method=='POST':
        comment_form = CommentForm(request.POST)

        if comment_form.is_valid():
            # 댓글 저장
            comment = comment_form.save(commit=False)
            comment.post = post
            comment.save()

            # 댓글 알림


            # 폼 초기화
            comment_form = CommentForm()
    else:
        comment_form = CommentForm()

    return render(request, 'blog/post_detail.html', {
        'post':post,
        'comment_form':comment_form,
    })

def post_form(request):
    if request.method=='POST':
        post_form = PostForm(request.POST, request.FILES)

        if post_form.is_valid():
            post = post_form.save(commit=False)
            post.save()

            return redirect('blog.views.post_list')
    else:
        post_form = PostForm()

    return render(request, 'blog/post_form.html',{
        'post_form':post_form
    })



# def add_comment()
