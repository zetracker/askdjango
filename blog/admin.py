from django.contrib import admin
from .models import Post, Comment, Tag

class PostAdmin(admin.ModelAdmin):
	list_display = ('id','title','get_tag_names', 'created_at', 'updated_at')

	def get_tag_names(self, post):
		return ','.join([tag.name for tag in post.tags.all()])


admin.site.register(Post, PostAdmin)


class CommentAdmin(admin.ModelAdmin):
	list_display = ('id', 'post_id', 'message')

admin.site.register(Comment, CommentAdmin)


admin.site.register(Tag)
