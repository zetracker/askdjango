from django.conf.urls import url
from django.http import JsonResponse

def post_list(request):
	post_list = [
		{'id':1, 'title':'title 1'},
		{'id':2, 'title':'title 2'},
	]
	return JsonResponse(post_list, safe=False)

urlpatterns = [
	url(r'^$', post_list),
]