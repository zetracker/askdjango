from django.conf.urls import url, include
from blog import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^posts/$', views.post_list),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail),
    url(r'^post/form/$',views.post_form),
]
