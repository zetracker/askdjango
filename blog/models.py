from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator


class Tag(models.Model):
    name = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    #author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blog_posts')
    title = models.CharField(max_length=100, validators=[MinLengthValidator(3)])
    content = models.TextField()
    photo = models.ImageField(blank=True, ) # upload_to=random_name_upload_to
    tags = models.ManyToManyField(Tag, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def random_name_upload_to(instance, filename):
        app_label = instance.__class__._meta.app_label


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comment_set')
    message = models.TextField(help_text='댓글을 입력해주세요', verbose_name='댓글')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.message[:50]
